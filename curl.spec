#Global macro or variable
%global libpsl_version %(pkg-config --modversion libpsl 2>/dev/null || echo 0)
%global libssh_version %(pkg-config --modversion libssh 2>/dev/null || echo 0)
%global openssl_version %(pkg-config --modversion openssl 2>/dev/null || echo 0)
%global _configure ../configure

Name:           curl
Version:        7.66.0
Release:        3
Summary:        Curl is used in command lines or scripts to transfer data
License:        MIT
URL:            https://curl.haxx.se/
Source:         https://curl.haxx.se/download/curl-%{version}.tar.xz

Patch1:         0001-curl-7.66.0-metalink-memleak.patch
Patch101:       0101-curl-7.32.0-multilib.patch
Patch102:       0102-curl-7.36.0-debug.patch
Patch103:       0103-curl-7.59.0-python3.patch
Patch104:       0104-curl-7.19.7-localhost6.patch
Patch105:       0105-curl-7.63.0-lib1560-valgrind.patch
Patch106:       0106-curl-fix-CVE-2019-15601.patch

BuildRequires:  automake brotli-devel coreutils gcc groff krb5-devel
BuildRequires:  libidn2-devel libmetalink-devel libnghttp2-devel libpsl-devel
BuildRequires:  libssh-devel make openldap-devel openssh-clients openssh-server
BuildRequires:  openssl-devel perl-interpreter pkgconfig python3-devel sed
BuildRequires:  stunnel zlib-devel gnutls-utils nghttp2 perl(IO::Compress::Gzip)
BuildRequires:  perl(Getopt::Long) perl(Pod::Usage) perl(strict) perl(warnings)
BuildRequires:  perl(Cwd) perl(Digest::MD5) perl(Exporter) perl(File::Basename)
BuildRequires:  perl(File::Copy) perl(File::Spec) perl(IPC::Open2) perl(MIME::Base64)
BuildRequires:  perl(Time::Local) perl(Time::HiRes) perl(vars)
%ifarch x86_64
BuildRequires:  valgrind
%endif

Requires:       libcurl = %{version}-%{release}
Provides:	curl-full = %{version}-%{release} webclient

%description
cURL is a computer software project providing a library (libcurl) and
command-line tool (curl) for transferring data using various protocols.

%package -n 	libcurl
Summary:	A library for getting files from web servers
Requires:	libssh >= %{libssh_version} libpsl >= %{libpsl_version}
Requires:       openssl-libs >= 1:%{openssl_version}
Provides:	libcurl-full = %{version}-%{release} 

%description -n libcurl
A library for getting files from web servers.

%package -n 	libcurl-devel
Summary:	Header files for libcurl
Requires:	libcurl = %{version}-%{release}
Provides:	curl-devel = %{version}-%{release}
Obsoletes:	curl-devel < %{version}-%{release}
	
%description -n libcurl-devel
Header files for libcurl.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

# make tests/*.py use Python 3
sed -e '1 s|^#!/.*python|#!%{__python3}|' -i tests/*.py

# regenerate Makefile.in files
aclocal -I m4
automake

printf "1112\n1455\n1801\n1900\n" >> tests/data/DISABLED

# adapt test 323 for updated OpenSSL
sed -e 's/^35$/35,52/' -i tests/data/test323

%build
install -d build-full
export common_configure_opts="--cache-file=../config.cache \
    --enable-symbol-hiding  --enable-ipv6  --enable-threaded-resolver \
    --with-gssapi  --with-nghttp2  --with-ssl \
    --with-ca-bundle=%{_sysconfdir}/pki/tls/certs/ca-bundle.crt"

%global _configure ../configure

# configure full build
(
    cd build-full
    %configure $common_configure_opts \
        --enable-ldap \
        --enable-ldaps \
        --enable-manual \
        --with-brotli \
        --with-libidn2 \
        --with-libmetalink \
        --with-libpsl \
        --with-libssh
)

sed -e 's/^runpath_var=.*/runpath_var=/' \
    -e 's/^hardcode_libdir_flag_spec=".*"$/hardcode_libdir_flag_spec=""/' \
    -i build-full/libtool

%make_build V=1 -C build-full

%install
rm -f ${RPM_BUILD_ROOT}%{_libdir}/libcurl.{la,so}

# install libcurl.m4 for devel
install -D -m 644 docs/libcurl/libcurl.m4 $RPM_BUILD_ROOT%{_datadir}/aclocal/libcurl.m4

# curl file install
cd build-full
%make_install

# install zsh completion for curl
LD_LIBRARY_PATH="$RPM_BUILD_ROOT%{_libdir}:$LD_LIBRARY_PATH" %make_install -C scripts

# do not install /usr/share/fish/completions/curl.fish which is also installed
# by fish-3.0.2-1.module_f31+3716+57207597 and would trigger a conflict
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/fish

rm -f ${RPM_BUILD_ROOT}%{_libdir}/libcurl.a
rm -rf ${RPM_BUILD_ROOT}%{_libdir}/libcurl.la

%ldconfig_scriptlets

%ldconfig_scriptlets -n libcurl

%files
%defattr(-,root,root)
%license COPYING
%{_bindir}/curl
%{_datadir}/zsh

%files -n libcurl
%defattr(-,root,root)
%{_libdir}/libcurl.so.4
%{_libdir}/libcurl.so.4.[0-9].[0-9]

%files -n libcurl-devel
%defattr(-,root,root)
%doc docs/examples/*.c docs/examples/Makefile.example docs/INTERNALS.md
%doc docs/CONTRIBUTE.md docs/libcurl/ABI
%{_bindir}/curl-config*
%{_includedir}/curl
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/aclocal/libcurl.m4

%files help
%defattr(-,root,root)
%doc CHANGES README*
%doc docs/BUGS docs/FAQ docs/FEATURES docs/RESOURCES
%doc docs/TheArtOfHttpScripting docs/TODO
%{_mandir}/man1/curl.1*
%{_mandir}/man1/curl-config.1*
%{_mandir}/man3/*

%changelog
* Thu Apr 9 2020 songnannan <songnannan2@huawei.com> - 7.66.0-3
- split out the libcurl and libcurl-devel package 

* Tue Mar 17 2020 chenzhen <chenzhen44@huawei.com> - 7.66.0-2
- Type:cves
- ID:CVE-2019-15601
- SUG:NA
- DESC:fix CVE-2019-15601

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.66.0-1
- update to 7.66.0

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.61.1-4
- Type:cves
- ID:CVE-2019-5481 CVE-2019-5482
- SUG:NA
- DESC:fix CVE-2019-5481 CVE-2019-5482

* Wed Sep 18 2019 guanyanjie <guanyanjie@huawei.com> - 7.61.1-3
- Init for openEuler
